# coding=utf-8
import json
import threading
import os
import time
from bme280 import read_sensor_data, is_rpi
from oled import display_temp_humidity

if is_rpi():
    import RPi.GPIO as GPIO


cur_dir = os.path.realpath(os.path.dirname(__file__))

c = json.loads(open(os.path.join(cur_dir, 'config.json')).read())


class SmartRPi(threading.Thread):

    b_stop = threading.Event()

    def __init__(self):
        super(SmartRPi, self).__init__()
        self.b_stop.clear()

    def run(self):
        while not self.b_stop.isSet():
            s_time = time.time()
            data = read_sensor_data()
            if data is None:
                time.sleep(1)
                continue
            h = data['humidity']
            if h < c['Humidifier']['On']:
                print('Humidity({}%) is lower than {}%, turning humidifier on...'.format(h, c['Humidifier']['On']))
                if is_rpi():
                    GPIO.output(c['Humidifier']['Pin'], GPIO.HIGH)
            elif h > c['Humidifier']['Off']:
                print('Humidity({}%) is higher than {}%, turning humidifier off...'.format(h, c['Humidifier']['Off']))
                if is_rpi():
                    GPIO.output(c['Humidifier']['Pin'], GPIO.LOW)

            if h > c['Dehumidifier']['On']:
                print('Humidity({}%) is higher than {}%, turning dehumidifier on...'.format(h, c['Dehumidifier']['On']))
                if is_rpi():
                    GPIO.output(c['Dehumidifier']['Pin'], GPIO.HIGH)
            elif h < c['Dehumidifier']['Off']:
                print('Humidity({}%) is lower than {}%, turning dehumidifier off..'.format(h, c['Dehumidifier']['Off']))
                if is_rpi():
                    GPIO.output(c['Dehumidifier']['Pin'], GPIO.LOW)

            t = data['temp_f']
            if t < c['Heating']['On']:
                print('Temperature({}°F) is lower than {}°F, turning heater on...'.format(t, c['Heating']['On']))
                if is_rpi():
                    GPIO.output(c['Heating']['Pin'], GPIO.HIGH)
            elif t > c['Heating']['Off']:
                print('Temperature({}°F) is higher than {}°F, turning heater off...'.format(t, c['Heating']['Off']))
                if is_rpi():
                    GPIO.output(c['Heating']['Pin'], GPIO.LOW)

            if t > c['Cooling']['On']:
                print('Temperature({}°F) is higher than {}°F, turning cooler on...'.format(t, c['Cooling']['On']))
                if is_rpi():
                    GPIO.output(c['Cooling']['Pin'], GPIO.HIGH)
            elif t < c['Cooling']['Off']:
                print('Temperature({}°F) is lower than {}°F, turning cooler off...'.format(t, c['Cooling']['Off']))
                if is_rpi():
                    GPIO.output(c['Cooling']['Pin'], GPIO.LOW)
            print('')
            display_temp_humidity(temperature=t, humidity=h)
            elapsed = time.time() - s_time
            time.sleep(max(0, c['Interval'] - elapsed))

    def stop(self):
        self.b_stop.set()


if __name__ == '__main__':

    app = SmartRPi()

    if is_rpi():
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(c['Humidifier']['Pin'], GPIO.OUT)
        GPIO.setup(c['Dehumidifier']['Pin'], GPIO.OUT)
        GPIO.setup(c['Cooling']['Pin'], GPIO.OUT)
        GPIO.setup(c['Heating']['Pin'], GPIO.OUT)

    try:
        app.start()
    except KeyboardInterrupt:
        app.stop()
