import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


def is_rpi():
    return os.name == 'posix' and 'arm' in os.uname()[4]


if is_rpi():

    import Adafruit_SSD1306

    # 128x64 display with hardware I2C:
    disp = Adafruit_SSD1306.SSD1306_128_32(rst=None, i2c_address=0x3C)

    disp.begin()

    # Clear display.
    disp.clear()
    disp.display()


def display_temp_humidity(temperature, humidity):
    # Create blank image for drawing.
    # Make sure to create image with mode '1' for 1-bit color.
    width = disp.width if is_rpi() else 128
    height = disp.height if is_rpi() else 32
    image = Image.new('1', (width, height))

    # Get drawing object to draw on image.
    draw = ImageDraw.Draw(image)

    # Draw a black filled box to clear the image.
    draw.rectangle((0, 0, width, height), outline=0, fill=0)

    # Load default font.
    font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMono.ttf', 16)

    draw.text((3, 7), "{}°F {}%".format(temperature, humidity), font=font, fill=255)

    if is_rpi():
        # Display image.
        disp.image(image)

        disp.display()
    else:
        image.show()


if __name__ == '__main__':

    display_temp_humidity(45.4, 34.2)
