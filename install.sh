#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y git build-essential
sudo apt-get install -y python3 libpython3-dev python3-pip

cd /tmp
git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
cd Adafruit_Python_GPIO
sudo python3 setup.py install

cd /tmp
git clone https://github.com/adafruit/Adafruit_Python_BME280
cd Adafruit_Python_BME280
sudo python3 setup.py install

echo "Installing OLED driver"
sudo apt-get install python3-smbus
sudo pip3 install Pillow

cd /tmp
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python3 setup.py install

sudo sed -i -- "s/^exit 0/(cd \/home\/pi\/RPi-BME280 \&\& python3 main.py)\&\\nexit 0/g" /etc/rc.local
